EDITS + NOTES FOR FINAL GERBER SUBMISSION

1) REDUNDANCIES
Extended parts are: 
	MOSFET Q1
	Battery charger IC U2
	Battery protector IC U3
	Battery protector IC U14 (ONE OF THESE EITHER U3 OR U14 is DNP ON PRODUCTION. ONLY TAKE BOTH IF BUDGET ALLOWS)

2) TEST POINTS

ALSO--------> ADD 0Ω resistors EVERYWHERE.

3) IMPORTANT 0Ω NOTES
Leave R23 disconnected by default.

4) BOM NOTES
U14 is new and extended. Uses HY2113KB5b.


_____________________________________________________________________________________________________


EDIT NOTES:	
	-Voltage regulator that can step up
	-UVLO circuit
	-CHECK UVLO CCT Behaviours

UVLO CIRCUIT
Zener diode is at https://datasheet.lcsc.com/lcsc/1811142211_ST-Semtech-ZMM3V3-M_C8056.pdf
The voltage that we want to shut off at is around VBAT = 3.3V
Therefore Vz = 3V3, we will use the ZMM 3V3 diode
NEED help choosing zener resistor value
CHECK THE UVLO RESISTOR VALUES

--OVERALL POWER MODULE SUBSYSTEM PARTS--

The power module subsystem consists of the following elements:
	-USB input circuit
		-Supplies power to a 5V line, from which the battery charger circuit recieves power
	
	-Battery charger circuit

	-LI battery interface to charger and power line of the rest of the system
	
	-Battery to 3V3 voltage regulator
		-The battery will supply 3V7 to this circuit. This must be regulated down to 3v3
		-Supplies the 3v3 line from which the remainder of the HAT runs

NOTES: There must be input voltage polarity protection (for charging battery I think), battery polarity protection (from battery to rest
of cct), and battery under-voltage cutout protection.



------------OLD BEFORE 11/03/2023---------------------------------------------------------------------------------------------------------------------


NOTE: Still to add: Under voltage cutoff protection
			The battery module I guess?

--BATTERY CHARGING IC--

The charging module IC needs some kind of shutoff protection between battery and the load to prevent "Trickle charge" (Apparently bad)

Easiest method is to DISABLE THE LOAD during charge: 
	--DISADVANTAGE is that you cannot use the system AND charge at the same time.
	--ADVANTAGE is it is easy and very cheap, and wont reduce charging time.
		There is also no need to design the overall circuit to be able to operate on both battery power and charging power, you only 
		have to account for how the power will be supplied from the battery.
	--Doable using a P-channel MOSFET between the output of protected battery cell/system load and the charging power input of the 
	charger.
		Charger = on >>> MOSFET is closed.

Another method -- Investigate POWER PATH charging circuits. It is a standard power IC that allows the battery to charge and for the system
to run by REGULATING the input current ratio of current supplied to the battery vs current supplied to the current supplied to the system.

Battery charging PARAMETERS:
	Can be charged at initial currents up to the Ah rating (This rate is 1C rate)
	1C is DANGEROUS. Use 0,5C
	NEEDS auto cutoff at full charge level, Constant V and I input to the battery.
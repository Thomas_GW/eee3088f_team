/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  *
  *Authors Thomas, Harry, Janna
  *This is the working copy thomas dont loose it
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define EEPROM_ADDR 0xA0

//Slave addresses
#define LIGHT_ADDR (0x29 << 1)

//Control
#define LIGHT_CONTR (0x80)
#define LIGHT_CONTR_ACTIVE (0x1)

#define LIGHT_MEAS_RATE (0x85)
#define LIGHT_MEAS_RATE_100ms_50ms (0x00)

#define LIGHT_INTERRUPT (0x8F)
#define LIGHT_INTERRUPT_INTERRUPT_ON_MEASURE (0b0001010)

#define LIGHT_STATUS (0x8C)
#define LIGHT_STATUS_NEW_DATA (0x04)

//Data
#define LIGHT_DATA_1_0 (0x88)
#define LIGHT_DATA_1_1 (0x89)
#define LIGHT_DATA_0_0 (0x8A)
#define LIGHT_DATA_0_1 (0x8B)
//*LIGHT SENSOR END */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */
/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef hadc;

I2C_HandleTypeDef hi2c1;

RTC_HandleTypeDef hrtc;

UART_HandleTypeDef huart1;

/* USER CODE BEGIN PV */
//used by the GPIO to determine what stage it is in
int start = 0;


//this is the error code manager
HAL_StatusTypeDef ret;

uint8_t toRead[64];

//these varibles are used to print date and times
char time[30];
char date[30];
char dateAndTime[60];

//this is the data that will be written and saved to eeprom
uint8_t SendData[64];


uint8_t digitalData;


uint8_t pageNumber;

RTC_TimeTypeDef sTime;
RTC_DateTypeDef sDate;



//the addresses of the Light sensor

//static const uint8_t Light_ADDR = 0x29<<1; // Use 8-bit address

//static const uint8_t Light_ALS_CONTR = 0x80;

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
static void MX_GPIO_Init(void);
static void MX_USART1_UART_Init(void);
static void MX_I2C1_Init(void);
static void MX_RTC_Init(void);
static void MX_ADC_Init(void);
/* USER CODE BEGIN PFP */
uint16_t AnalogeRead();
void GUI();
void EEPROM_Write(uint8_t);
void EERPOm_Read(uint8_t);
void EEPROM_DataPos();
void getTime();
void getPageNumber();
void formatData();
uint16_t LightData();


/*
 * Displays basic GPio on start up only
 */
void GUI()
{
 //starting the GUI procedure will make this a function when done
	  if(start ==0){
		  uint8_t hello[] = "hello \r\n";
		  HAL_UART_Transmit(&huart1,hello,strlen((char*)hello),10);
		  HAL_Delay(2000);
		  start = 0;
	  }
}

/*
 * Reads the Analogue data and prints the message, will need this to work more like the get date function
 */

uint16_t AnalogeRead()
{
	  HAL_ADC_Start(&hadc);
	  HAL_Delay(100);

	  HAL_ADC_PollForConversion(&hadc, 100);
	  uint16_t adc_val = HAL_ADC_GetValue(&hadc); // get the adc value

	  HAL_ADC_Stop(&hadc); // stop adc
	  return adc_val;
}


/*
 * Formats the data so that it can be printed out
 */
void formatData()
{
	strcpy((char*)SendData, "");
	uint16_t adc_val = AnalogeRead();
	char doorStatus[64];
	getTime();
	if(adc_val < 2000 || adc_val >2300){
	  sprintf(doorStatus,"Door locked and ");
	}
	else{
	  sprintf(doorStatus,"Door unlocked and ");
	}

	uint16_t digital_val = LightData();
	if(digital_val < 5){
		strcat(doorStatus,"closed \r\n");
	}
	else{
		strcat(doorStatus,"open \r\n");
	}

	strcat(doorStatus,dateAndTime);
	strcpy((char*)SendData,doorStatus);
}
/*
 * sends the formatted data to UART
 */

void sendData()
{
	HAL_UART_Transmit(&huart1,SendData,strlen((char*)SendData),100);
}

void EEPROM_Write(uint8_t page)
{
	uint8_t currentPage = page;
	currentPage <<6;
	HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, currentPage, 2, SendData, 64, 1000);
	HAL_Delay(50);

}
void EEPROM_Read(uint8_t page)
{
	uint8_t currentPage = page;
	currentPage<<6;
	HAL_I2C_Mem_Read(&hi2c1, EEPROM_ADDR, currentPage,2 , toRead, 64, 1000);
}



void eraseEEPROM()
{
	uint8_t data[64];
	memset(data,0x00,64);
	for(int i = 1; i<511; i++)
	{
		ret = HAL_I2C_Mem_Write(&hi2c1, EEPROM_ADDR, i<<6, 2, data, 64, 1000);
	}
}



uint16_t LightData()
{

	uint8_t buf[1];
	HAL_StatusTypeDef ref = HAL_I2C_IsDeviceReady(&hi2c1, LIGHT_ADDR, 2, 1000);

	buf[0] = LIGHT_CONTR_ACTIVE;
	ref = HAL_I2C_Mem_Write(&hi2c1, LIGHT_ADDR, LIGHT_CONTR, 1, buf, 1, 1000);
	//this works

	//wait until new_data is high
	ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_STATUS, 1, buf, 1, 1000);

		while ((buf[0] & LIGHT_STATUS_NEW_DATA) == 0b0) {
			ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_STATUS, 1, buf, 1, 1000);
		}
	//readData
	ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_DATA_1_0, 1, buf, 1, 1000);
	uint8_t lowwerData = buf[0];

	ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_DATA_1_1, 1, buf, 1, 1000);
	lowwerData |= (buf[0] << 8);

	ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_DATA_0_0, 1, buf, 1, 1000);
	uint16_t digitalData = buf[0];

	ref = HAL_I2C_Mem_Read(&hi2c1, LIGHT_ADDR, LIGHT_DATA_0_1, 1, buf, 1, 1000);
	digitalData |= (buf[0] << 8);

	return digitalData;

}


/*
 * This will get the date and time from the RTC and save it to the date and time vars
 */


void getTime()
{
	  HAL_RTC_GetTime(&hrtc, &sTime, RTC_FORMAT_BIN);
	  HAL_RTC_GetDate(&hrtc, &sDate, RTC_FORMAT_BIN);
	  sprintf(dateAndTime,"Date: %02d.%02d.%02d\t Time: %02d.%02d.%02d\r\n",sDate.Date,sDate.Month,sDate.Year,sTime.Hours,sTime.Minutes,sTime.Seconds);


}
/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_USART1_UART_Init();
  MX_I2C1_Init();
  MX_RTC_Init();
  MX_ADC_Init();
  /* USER CODE BEGIN 2 */

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */

  HAL_ADCEx_Calibration_Start(&hadc);


  HAL_Delay(100);
  while (1)
  {
	  //void GUI();
	  uint8_t cnt = 0;
	  HAL_UART_Transmit(&huart1,SendData,strlen((char*)SendData),100);

	  HAL_Delay(100);
	  while(cnt<2) //!(HAL_GPIO_ReadPin(GPIOB,GPIO_PIN_0)
	  {
		  cnt++;
		  formatData();
		  EEPROM_Write(cnt);
		  start = 0;
	  }
	  while(cnt>0)
	  {

		  EEPROM_Read(cnt);
		  cnt--;

	  }
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};
  RCC_PeriphCLKInitTypeDef PeriphClkInit = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI|RCC_OSCILLATORTYPE_HSI14
                              |RCC_OSCILLATORTYPE_LSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSI14State = RCC_HSI14_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.HSI14CalibrationValue = 16;
  RCC_OscInitStruct.LSIState = RCC_LSI_ON;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_NONE;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_HSI;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_0) != HAL_OK)
  {
    Error_Handler();
  }
  PeriphClkInit.PeriphClockSelection = RCC_PERIPHCLK_USART1|RCC_PERIPHCLK_I2C1
                              |RCC_PERIPHCLK_RTC;
  PeriphClkInit.Usart1ClockSelection = RCC_USART1CLKSOURCE_PCLK1;
  PeriphClkInit.I2c1ClockSelection = RCC_I2C1CLKSOURCE_HSI;
  PeriphClkInit.RTCClockSelection = RCC_RTCCLKSOURCE_LSI;
  if (HAL_RCCEx_PeriphCLKConfig(&PeriphClkInit) != HAL_OK)
  {
    Error_Handler();
  }
}

/**
  * @brief ADC Initialization Function
  * @param None
  * @retval None
  */
static void MX_ADC_Init(void)
{

  /* USER CODE BEGIN ADC_Init 0 */

  /* USER CODE END ADC_Init 0 */

  ADC_ChannelConfTypeDef sConfig = {0};

  /* USER CODE BEGIN ADC_Init 1 */

  /* USER CODE END ADC_Init 1 */

  /** Configure the global features of the ADC (Clock, Resolution, Data Alignment and number of conversion)
  */
  hadc.Instance = ADC1;
  hadc.Init.ClockPrescaler = ADC_CLOCK_ASYNC_DIV1;
  hadc.Init.Resolution = ADC_RESOLUTION_12B;
  hadc.Init.DataAlign = ADC_DATAALIGN_RIGHT;
  hadc.Init.ScanConvMode = ADC_SCAN_DIRECTION_FORWARD;
  hadc.Init.EOCSelection = ADC_EOC_SINGLE_CONV;
  hadc.Init.LowPowerAutoWait = DISABLE;
  hadc.Init.LowPowerAutoPowerOff = DISABLE;
  hadc.Init.ContinuousConvMode = DISABLE;
  hadc.Init.DiscontinuousConvMode = DISABLE;
  hadc.Init.ExternalTrigConv = ADC_SOFTWARE_START;
  hadc.Init.ExternalTrigConvEdge = ADC_EXTERNALTRIGCONVEDGE_NONE;
  hadc.Init.DMAContinuousRequests = DISABLE;
  hadc.Init.Overrun = ADC_OVR_DATA_PRESERVED;
  if (HAL_ADC_Init(&hadc) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure for the selected ADC regular channel to be converted.
  */
  sConfig.Channel = ADC_CHANNEL_0;
  sConfig.Rank = ADC_RANK_CHANNEL_NUMBER;
  sConfig.SamplingTime = ADC_SAMPLETIME_1CYCLE_5;
  if (HAL_ADC_ConfigChannel(&hadc, &sConfig) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN ADC_Init 2 */

  /* USER CODE END ADC_Init 2 */

}

/**
  * @brief I2C1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_I2C1_Init(void)
{

  /* USER CODE BEGIN I2C1_Init 0 */

  /* USER CODE END I2C1_Init 0 */

  /* USER CODE BEGIN I2C1_Init 1 */

  /* USER CODE END I2C1_Init 1 */
  hi2c1.Instance = I2C1;
  hi2c1.Init.Timing = 0x2000090E;
  hi2c1.Init.OwnAddress1 = 0;
  hi2c1.Init.AddressingMode = I2C_ADDRESSINGMODE_7BIT;
  hi2c1.Init.DualAddressMode = I2C_DUALADDRESS_DISABLE;
  hi2c1.Init.OwnAddress2 = 0;
  hi2c1.Init.OwnAddress2Masks = I2C_OA2_NOMASK;
  hi2c1.Init.GeneralCallMode = I2C_GENERALCALL_DISABLE;
  hi2c1.Init.NoStretchMode = I2C_NOSTRETCH_DISABLE;
  if (HAL_I2C_Init(&hi2c1) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Analogue filter
  */
  if (HAL_I2CEx_ConfigAnalogFilter(&hi2c1, I2C_ANALOGFILTER_ENABLE) != HAL_OK)
  {
    Error_Handler();
  }

  /** Configure Digital filter
  */
  if (HAL_I2CEx_ConfigDigitalFilter(&hi2c1, 0) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN I2C1_Init 2 */

  /* USER CODE END I2C1_Init 2 */

}

/**
  * @brief RTC Initialization Function
  * @param None
  * @retval None
  */
static void MX_RTC_Init(void)
{

  /* USER CODE BEGIN RTC_Init 0 */

  /* USER CODE END RTC_Init 0 */

  RTC_TimeTypeDef sTime = {0};
  RTC_DateTypeDef sDate = {0};

  /* USER CODE BEGIN RTC_Init 1 */

  /* USER CODE END RTC_Init 1 */

  /** Initialize RTC Only
  */
  hrtc.Instance = RTC;
  hrtc.Init.HourFormat = RTC_HOURFORMAT_24;
  hrtc.Init.AsynchPrediv = 127;
  hrtc.Init.SynchPrediv = 255;
  hrtc.Init.OutPut = RTC_OUTPUT_DISABLE;
  hrtc.Init.OutPutPolarity = RTC_OUTPUT_POLARITY_HIGH;
  hrtc.Init.OutPutType = RTC_OUTPUT_TYPE_OPENDRAIN;
  if (HAL_RTC_Init(&hrtc) != HAL_OK)
  {
    Error_Handler();
  }

  /* USER CODE BEGIN Check_RTC_BKUP */

  /* USER CODE END Check_RTC_BKUP */

  /** Initialize RTC and set the Time and Date
  */
  sTime.Hours = 0x0;
  sTime.Minutes = 0x0;
  sTime.Seconds = 0x0;
  sTime.DayLightSaving = RTC_DAYLIGHTSAVING_NONE;
  sTime.StoreOperation = RTC_STOREOPERATION_RESET;
  if (HAL_RTC_SetTime(&hrtc, &sTime, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  sDate.WeekDay = RTC_WEEKDAY_THURSDAY;
  sDate.Month = RTC_MONTH_MAY;
  sDate.Date = 0x18;
  sDate.Year = 0x23;

  if (HAL_RTC_SetDate(&hrtc, &sDate, RTC_FORMAT_BCD) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN RTC_Init 2 */

  /* USER CODE END RTC_Init 2 */

}

/**
  * @brief USART1 Initialization Function
  * @param None
  * @retval None
  */
static void MX_USART1_UART_Init(void)
{

  /* USER CODE BEGIN USART1_Init 0 */

  /* USER CODE END USART1_Init 0 */

  /* USER CODE BEGIN USART1_Init 1 */

  /* USER CODE END USART1_Init 1 */
  huart1.Instance = USART1;
  huart1.Init.BaudRate = 9600;
  huart1.Init.WordLength = UART_WORDLENGTH_8B;
  huart1.Init.StopBits = UART_STOPBITS_1;
  huart1.Init.Parity = UART_PARITY_NONE;
  huart1.Init.Mode = UART_MODE_TX_RX;
  huart1.Init.HwFlowCtl = UART_HWCONTROL_NONE;
  huart1.Init.OverSampling = UART_OVERSAMPLING_16;
  huart1.Init.OneBitSampling = UART_ONE_BIT_SAMPLE_DISABLE;
  huart1.AdvancedInit.AdvFeatureInit = UART_ADVFEATURE_NO_INIT;
  if (HAL_UART_Init(&huart1) != HAL_OK)
  {
    Error_Handler();
  }
  /* USER CODE BEGIN USART1_Init 2 */

  /* USER CODE END USART1_Init 2 */

}

/**
  * @brief GPIO Initialization Function
  * @param None
  * @retval None
  */
static void MX_GPIO_Init(void)
{
  GPIO_InitTypeDef GPIO_InitStruct = {0};
/* USER CODE BEGIN MX_GPIO_Init_1 */
/* USER CODE END MX_GPIO_Init_1 */

  /* GPIO Ports Clock Enable */
  __HAL_RCC_GPIOA_CLK_ENABLE();
  __HAL_RCC_GPIOB_CLK_ENABLE();

  /*Configure GPIO pin Output Level */
  HAL_GPIO_WritePin(GPIOB, GPIO_PIN_15, GPIO_PIN_RESET);

  /*Configure GPIO pin : PB0 */
  GPIO_InitStruct.Pin = GPIO_PIN_0;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

  /*Configure GPIO pin : PB15 */
  GPIO_InitStruct.Pin = GPIO_PIN_15;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_PP;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_LOW;
  HAL_GPIO_Init(GPIOB, &GPIO_InitStruct);

/* USER CODE BEGIN MX_GPIO_Init_2 */
/* USER CODE END MX_GPIO_Init_2 */
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

# EEE3088F_2023_Team1 README
## Introduction
This repository contains all of the circuit designs, board layouts, and firmware that is required to get started with the EEE3088F_2023_Team1 door status detector HAT for the STM32 microcontroller. This HAT will be able to detect and log if a door is locked or not and detect ambient lighting, and send that information to the user via wifi.

This getting started tutorial will help you through:
1. Cloning the git repository.
2. Preparing the hardware to recieve the firmware.
3. Uploading the firmware from the repository to the HAT.
4. Performing a software check.

Once finished, the device will be ready to start monitoring the status of any door that you choose to install it onto!

## What you will need
- An STM32 microcontroller with a 48 pin male connector that is able to fit in the board designs presented.
- The EEE3088F_2023_Team1 STM32 door status detector HAT.
- A usb to ucb c cable for interfacing the HAT with a computer.
- Git bash installed on your machine of choice, as well as a [Gitlab](https://gitlab.com) account.

## How to use this repo
The files needed to complete this project are located on a git repository. In order to make the files accessible on your local machine, you need to set up a local repository (an offline copy) of this one.

1. Initiate a github repository using the *git bash* console:
```bash
git init
```
2. Clone this public repository recursively using:
```bash
git clone https://gitlab.com/Thomas_GW/EEE3088F_Team1.git --recursive
```
3. The local repository is set up! Take a moment to browse the files and acquaint yourself with everything if you would like to.

4. Be sure to "pull" updates from the  public repository using the *git pull* command.

All the folders contain the various sections of this project, and they will remain updated over time.

## Setting up the HAT hardware
The only assembly required for the STM HAT board is inserting the STM32 microcontroller into the board's slot. Simply alighn the pins and
# HARRY Just put images and accurately describe the plugin process
The hardware is ready to have the firmware installed.

## Installing the firmware
Installing the firmware.

## Performing a simple software check
Performing a simple software check.

# OLD BELOW HERE
# How does this project work?
1. The target is a STM32 micro controller.
2. The PCB developed will contain every component needed to detect the door state (open , closed unlocked, closed and locked) and ambient lighting and log that data.

# Structure:
    PCB: The KiCAD Project, Schematics & Layout and Project Libraries.
    Firmware: Any software developed for the hardware or instructions on where to find relevant sofware.
    Documentation: Project documentation and component datasheets.
    Production_costs: Budjecting, production houses and all the manufacturing that will be outsourced.
    Simulation: Any simulation files (eg SPICE) or design stage generated results (Eg matlab or excel).
    3D_models: Any 3D models or mechanical designs for enclosures or support.
    Members: Contains Names of members and the roles they are responsible for.

# License
[Creative Commons Attribution 4.0 International license](https://choosealicense.com/licenses/cc-by-4.0/)
